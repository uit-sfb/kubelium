# Docs

The docs are built with [mkdocs](https://www.mkdocs.org/) and automatically served
on [readthedocs](https://readthedocs.org/) after each git commit at:
https://metakube.readthedocs.io

## mkdocs

Continuously build the doc and serve locally with:
```
mkdocs serve
```

Build the docs with:
```
mkdocs build
```
