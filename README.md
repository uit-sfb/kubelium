# Kubelium

[![Documentation Status](https://readthedocs.org/projects/kubelium/badge/?version=latest)](https://kubelium.readthedocs.io/en/latest/?badge=latest)

*Kubelium* is a workflow orchestrator and executor for [Kubernetes](https://kubernetes.io/).

Its main objective is to speed up the lifecycle iterations of complex workflow design,
from prototyping to production in the cloud.
Using Kubernetes as container orchestrator, Kubelium is designed for scalability.
Start by prototyping your workflow on your laptop, then leverage the power of any computer cluster
to execute your workflow without the overhead of configuring and maintaining a complex IT infrastructure.


## Features

## Getting started

## Documentation

[Official documentation](https://kubelium.readthedocs.io/en/latest/)
