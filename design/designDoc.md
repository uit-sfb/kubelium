# Design document

## Features

- Execution of containerized workflow on k8s
  - includes request reception (queuing)
  - a request is the combination of
    - a workflow description (multiple can be supported, but to start with it is Kubelium Worlfow Language)
    - input data (file)
    - parameters format depends on the workflow language
- Products are stored in an Object Store, available for user retrieval
  - optional ttl
- Control
  - Cancel workflow
  - Retry workflow
- Real-time status
  - workflow, job and task status
  - logs & exit codes 
- Security
  - user segregation: inputs, statuses and products belong the user having submitted the workflow request


### Design criteria

- C1: Make use of data localism
- C2: Corollary: do not separate workflow execution from task execution
- C3: No modification to user code
- C4: Corollary: Included storage to object store
- C5: Do not require Dynamic provisioning (NIRD)
- C6: Support for dynamic parallelism
  - Some tools are directly parallelizable by splitting their inputs.
  If so we may want to make use of this feature in case the k8s nodes do not have enough resources to run the unparallelized dataset.
  - Some tools may output an arbitrary number of files that we may want to process in parallel
- C7: Make use of [WES API](https://github.com/ga4gh/workflow-execution-service-schemas)?
- C8: Deployable on any k8s cluster without modification
- C9: Handle workflow gracefully failure
- C10: Do not recompute (important to handle gracefully errors in a context of computationally expensive workflows)


### Existing SW

[TESK](https://github.com/EMBL-EBI-TSI/TESK) is an implementation of WES and TES on Kubernetes.
It does not seem to see any active development.
In addition, it does not meet C1, C2, C4, C5, C6, C8, C9, C10

[Kubeflow pipeline](https://github.com/kubeflow/pipelines) is a ML workflow orchestrator on Kubernetes.
Backed by [Argo Workflow](https://github.com/argoproj/argo), which requires very large monolithic workflow definition files
impractical to work with directly.
Pipelines are defined in Python.

[Pachyderm](https://github.com/pachyderm/pachyderm) is another ML workflow orchestrator on Kubernetes.
Despite a very active and helpful development team, failure to successfully deploy a production workflow orchestrator backed by Pachyderm is the starting point of this project.
- https://github.com/pachyderm/pachyderm/issues/4622
- https://github.com/pachyderm/pachyderm/issues/4566
- https://github.com/pachyderm/pachyderm/issues/4607
- https://github.com/pachyderm/pachyderm/issues/4071

Pachyderm has an interesting dynamic workflow definition where each stage takes for input a set of files from other stages.
This set of input is dynamic in the sense that it is defined by *glob patterns*.


## Design

### Model

#### Implication of data locality

Kubelium is meant to execute workflow (called *analysis*) on potentially very large datasets.
For this reason, it seems wise to utilize data locality as much as possible to avoid having to send large datasets
back and forth between task executors.

Therefore, the approach taken by TESK of dividing workflow into small unit of work straight away, although
having the advantage of being simpler, is not suitable with our requirements.
Instead, an entity in charge of executing an analysis, called *Foreman*, should act at workflow level by facilitation the execution 
analyses by providing an execution context by mean of an exclusive temporary local storage.

The Foreman follows TESK's model in that it can divide a workflow into smaller units of work called *tasks*.

#### 3-layered work granularity

Although a Kubernetes cluster may offer an arbitrarily large amount of resources, in practice a task cannot pass the boundaries
of the node in the cluster having the largest amount of available resources. A task requiring too much resources will not be schedulable.
Therefore, if a very resource-consuming task can be run concurrently by splitting the inputs, it should be done in order
to ensure that this task will be schedulable on the cluster. The amount of parallelism required depends of the nature of the task, the
cluster specification and the cluster workload.

With this, let us introduce a third and smallest unit of work of our model called a *job*.
Jobs are indivisible -- or atomic, which is where the name *Kubelium* comes from: a pseudo elementary
element that could fit in Mendeleiev periodic table.

The figure below presents the proposed 3-layered work granularity model:

<img src="https://docs.google.com/drawings/d/e/2PACX-1vQ8SRUwBt-h_bPr_1ZA8IFGWPF_Re6QDkp-ud2qEIyXnLuFvGC-Y216306ebjoMhAygD6RyN184Qydq/pub?w=443&amp;h=479">

Note that the transformation from Analysis all the way to Jobs is performed by the Foreman.

#### Workflow definitions

Given the model presented above, it is necessary to use a representation dynamic enough to incorporate seamlessly
- the job parallelism
- tools that output an unknown amount of output files (such as binning tools for instance)

Pachyderm makes use of a very interesting workflow description based on *glob patterns*.
Kubelium should follow the same direction.


### Architecture

An overview of the architecture is presented hereafter.

<img src="https://docs.google.com/drawings/d/e/2PACX-1vRrZPzVby33Yon89D01LraH5PSfXKxa2P7sJHubc0ZB8B5ZyjQs7_REA7iVM5nYv287A0weEIDVYgUw/pub?w=1076&amp;h=643">

A Kubelium endpoint receives *Analysis requests* via a REST API, processes them (transforms them into 
a format understandable by the *Foremen*) and sends them to a unique queue as they arrive.

Each Foreman oversees the execution of one analysis at a time providing a new PV (or cleaned PV in case the
cluster does not support dynamic provisioning) put at disposition of the jobs to store their outputs.
The Foreman splits the analysis to a set of tasks statically, then, as each task becomes ready (it's inputs are ready),
each task yields a set of jobs.
The execution of the jobs is handed over to *workers* via a *job queue*.
The executors simply pulls a job from the queue and spawns the corresponding k8s job.
The Kubernetes jobs run un-supervised so a process in the Foreman polls each k8s Job instance, uploads the complete outputs and update the job/task/workflow status in the key-value store.

The reason why workers are needed, instead of letting the foremen spawn the jobs themselves, is to be able to control
the number of k8s job running simultaneously (to prevent overloading k8s itself or to fit into quotas).

The figure below presents a closer view of the foreman-worker interaction:

<img src="https://docs.google.com/drawings/d/e/2PACX-1vTb1iGbfaW5xXVuMJn3w5ppTBS0OLnXeKOfeFg2LlUG-_elt4Mh1mkfJ2PEr0gQ_95Tb-Y5ru3AsG8F/pub?w=745&amp;h=512">
